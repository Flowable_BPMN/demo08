package com.study.demo.test;

import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.Test;

import com.study.demo.config.BaseConfiguation;


/**
 *基于事件的网关--测试
 *
 */
public class EventbasedGatewayTest extends BaseConfiguation{

	/**
	 * 部署
	 */
	@Test
	public void deploy() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("eventbasedGateway")
													.name("eventbasedGateway")
													.addClasspathResource("process/基于事件的网关.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("流程ID: " + deploy.getId());
	}
	/**
	 * 启动流程实例 
	 * 
	 */
	@Test
	public void start() {
		String processDefinitionKey = "eventbasedGateway";
		runtimeService.startProcessInstanceByKey(processDefinitionKey);
	}
	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "172503";
		taskService.complete(taskId);
	}
	/**
	 * 20分钟
	 * @throws Exception 
	 */
	@Test
	public void sleep() throws Exception {
		Long  millis = 1200000l;
		Thread.sleep(millis);;
	}
}
