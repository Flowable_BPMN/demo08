package com.study.demo.test;

import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.Test;

import com.study.demo.config.BaseConfiguation;


/**
 *并行网关--测试
 *
 */
public class ParallelGatewayTest extends BaseConfiguation{

	/**
	 * 部署
	 */
	@Test
	public void deploy() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("forkJoin")
													.name("forkJoin")
													.addClasspathResource("process/并行网关.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("流程ID: " + deploy.getId());
	}
	/**
	 * 启动流程实例 
	 * 
	 */
	@Test
	public void start() {
		String processDefinitionKey = "forkJoin";
		runtimeService.startProcessInstanceByKey(processDefinitionKey);
	}
	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "172503";
		taskService.complete(taskId);
	}

}
